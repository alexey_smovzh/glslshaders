package shaders;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.VideoRecorderAppState;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.shape.Quad;
import com.jme3.system.AppSettings;

/**
 *
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 */
public class Main extends SimpleApplication {
    
    private Geometry plane;

    private static final int FPS = 20;
    
    private static final float WIDTH = 16f;
    private static final float HEIGHT = 9f;
    private static final float DISTANCE = 20f;
    

    @Override
    public void simpleInitApp() {
        
        setupCam();
//        setupRecord();
    
        Mesh quad = new Quad(WIDTH, HEIGHT);
        plane = new Geometry("Plane", quad);       
        plane.setLocalTranslation(new Vector3f(-WIDTH / 2, -HEIGHT / 2, 0f));
        
//        Material material = new Material(assetManager, "MatDefs/OldTVBubbles.j3md");
//        Material material = new Material(assetManager, "MatDefs/BlueWaves.j3md");        
//        Material material = new Material(assetManager, "MatDefs/Rectangles.j3md");                
//        Material material = new Material(assetManager, "MatDefs/BlueBubbles.j3md");                                
//        Material material = new Material(assetManager, "MatDefs/SlidingRectangles.j3md");                                
//        Material material = new Material(assetManager, "MatDefs/PaintFlow.j3md");                                
//        Material material = new Material(assetManager, "MatDefs/SimpleNoise.j3md");                                        
//        Material material = new Material(assetManager, "MatDefs/VolumetricCloud.j3md");                                        

        Material material = new Material(assetManager, "MatDefs/AuroraBorealis.j3md");                                              
//        Material material = new Material(assetManager, "MatDefs/AuroraBorealis2.j3md");                                      
        material.setVector2("Resolution", new Vector2f(this.settings.getWidth(), this.settings.getHeight()));
       
                
                
//        material.setTexture("Noise0", assetManager.loadTexture("Textures/noise0.png")); // aurora noise, only for AuroraBorealis2.j3md
//        material.setTexture("Noise1", assetManager.loadTexture("Textures/noise1.png")); // stars noise, only for AuroraBorealis2.j3md
        material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Color);  // only for AuroraBorealis.j3md
        plane.setQueueBucket(RenderQueue.Bucket.Translucent);                           // only for AuroraBorealis.j3md

        
        plane.setMaterial(material);

        rootNode.attachChild(plane);
    }


    @Override
    public void simpleUpdate(float tpf) {
                
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private void setupRecord() {
        
        stateManager.attach(new VideoRecorderAppState());
        
    }
    
    private void setupCam() {

        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(ColorRGBA.DarkGray);
        
        cam.setLocation(new Vector3f(0f, 0f, DISTANCE));
        
    }
    
    public static void main(String[] args) {
        
        Main app = new Main();
        
        AppSettings settings = new AppSettings(true);
//        settings.setResolution(540, 960);               
        settings.setResolution(960, 540);               
        settings.setFrameRate(FPS);
        
        app.setSettings(settings);
        app.setShowSettings(false);
        
        app.start();
    }
}



