package shaders;

import com.jme3.app.SimpleApplication;
import com.jme3.app.state.VideoRecorderAppState;
import com.jme3.asset.TextureKey;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.VertexBuffer.Type;
import com.jme3.system.AppSettings;
import com.jme3.util.BufferUtils;
import java.nio.FloatBuffer;

/**
 *
 * @author Alexey Smovzh <alexey.smovzh@gmail.com>
 */
public class AuroraVertex extends SimpleApplication {
    
    private Geometry plane;
    private boolean angleDir;                                                   // direction to rotate plane clockwise/conter
    
    private static final boolean CLOCKWISE = true;
    private static final boolean CONTERCLOCKWISE = false;

    private static final int FPS = 20;
    private static final float DISTANCE = 20f;
    
    private static final int TEXTURE_COUNT_U = 4;  
    private static final int TEXTURE_COUNT_V = 2;  
    private static final int NUM = 8;
    private static final int XY_OFFSET = 200;    
    private static final int Z_OFFSET = 2;
    private static final int ANGLE = 5;                                         // max plane angle in deg

    

    // http://www.paintshopprotutorials.co.uk/html/aurora_borealis_sky_gimp.html
    // http://gimp-tutorials.net/gimp_msnbc_effect/
    

    @Override
    public void simpleInitApp() {
        
        setupCam();
//        setupRecord();
    
        // Set direction to rotate plane
        angleDir = FastMath.rand.nextBoolean();
        
        Node m1 = (Node)assetManager.loadModel("Blender/aurora.j3o");
        Node m2 = (Node)m1.getChild("Plane1");
        plane = (Geometry)m2.getChild("Plane1");
        for(int i = 0; i < NUM; i++) {            
            emit((Geometry)plane.deepClone(), -(float)i * Z_OFFSET);
        }        
    }
    
    // Change uv mapping
    // http://hub.jmonkeyengine.org/t/jme-crashes-when-changing-uvs-at-runtime/17599    
    // offset = 1f / textures num in file by X axis * texture position in file
    // texture position numbers start from 0
    private void setUVmappingSingle(Geometry model, int num) {
        
        float offset = 1f / TEXTURE_COUNT_U * num;
        
        VertexBuffer uv = model.getMesh().getBuffer(Type.TexCoord);
        float[] uvArray = BufferUtils.getFloatArray((FloatBuffer)uv.getData());

        for(int i = 0; i < uvArray.length; i += 2) {
            float coord = uvArray[i+1] / TEXTURE_COUNT_U;
            uvArray[i+1] = coord + offset;

        }
        uv.updateData(BufferUtils.createFloatBuffer(uvArray));
    }

    // Change uv mapping for multicolumn texture
    private void setUVmappingMult(Geometry model, int num) {
        
        int row, column;
        
        if(num >= TEXTURE_COUNT_U) {
            row = num - TEXTURE_COUNT_U;
            column = num / TEXTURE_COUNT_U;
        } else {
            row = num;
            column = 0;
        }
        
        float offsetRow = 1f / TEXTURE_COUNT_U * row;
        float offsetCol = 1f / TEXTURE_COUNT_V * column;
        
        VertexBuffer uv = model.getMesh().getBuffer(Type.TexCoord);
        float[] uvArray = BufferUtils.getFloatArray((FloatBuffer)uv.getData());
        
        for(int i = 0; i < uvArray.length; i += 2) {
            
            float u = uvArray[i+1] / TEXTURE_COUNT_U;
            float v = uvArray[i] / TEXTURE_COUNT_V;
            uvArray[i+1] = u + offsetRow;
            uvArray[i] = v + offsetCol;            

        }        
        
        uv.updateData(BufferUtils.createFloatBuffer(uvArray));
    }
 
    // Return random float angle in range with random positive/negative value
    // only Y and Z axis
    private Vector3f randomAngle(){

        if(angleDir == CLOCKWISE) {
            return new Vector3f(0f, (float)FastMath.rand.nextInt(ANGLE), (float)FastMath.rand.nextInt(ANGLE));

        } else {
            return new Vector3f(0f, -(float)FastMath.rand.nextInt(ANGLE), -(float)FastMath.rand.nextInt(ANGLE));    
        }
    }
    
    
    // Emit aurora geometrics
    private void emit(Geometry model, float z) {

        // Randomly rotate aurora plane in Y, Z axis        
        Vector3f vector = randomAngle();
        model.setLocalRotation(new Quaternion().fromAngles(-FastMath.PI / 2, 
                                                           FastMath.DEG_TO_RAD * vector.y, 
                                                           FastMath.DEG_TO_RAD * vector.z));

        // Randomly change plane X, Y position
        float x = (float)FastMath.rand.nextInt(XY_OFFSET) / 10;
//        float y = (float)FastMath.rand.nextInt(XY_OFFSET / 4) / 10;
        
        model.setLocalTranslation(new Vector3f(x, 0f, z));

        
        // Set random texture by UV coordinates
        //setUVmappingSingle(model, FastMath.rand.nextInt(TEXTURE_COUNT_X));
        setUVmappingMult(model, FastMath.rand.nextInt(TEXTURE_COUNT_U * TEXTURE_COUNT_V));        
        
                
        Material material = new Material(assetManager, "MatDefs/AuroraVertex.j3md");
        material.setTexture("ColorMap", assetManager.loadTexture(new TextureKey("Blender/aurora2.png", false)));
        material.setVector2("Resolution", new Vector2f(this.settings.getWidth(), this.settings.getHeight()));
        material.setVector3("Color0", new Vector3f(0f, 1f, 0f));
        material.setVector3("Color1", new Vector3f(0f, 0f, 1f));        
        material.setFloat("Wave", FastMath.rand.nextFloat());
        material.setFloat("Speed", FastMath.rand.nextFloat() / 2);

        material.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off);
        material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Color);
        model.setQueueBucket(RenderQueue.Bucket.Translucent);                         

        model.setMaterial(material);

        rootNode.attachChild(model);

    }
    
    
    @Override
    public void simpleUpdate(float tpf) {
                
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    
    private void setupRecord() {
        
        stateManager.attach(new VideoRecorderAppState());
        
    }
    
    private void setupCam() {

        flyCam.setMoveSpeed(20f);
        viewPort.setBackgroundColor(new ColorRGBA(.004f, .052f, .152f, 1f));        
        
        cam.setLocation(new Vector3f(0f, 0f, DISTANCE));
        
    }
    
    public static void main(String[] args) {
        
        AuroraVertex app = new AuroraVertex();
        
        AppSettings settings = new AppSettings(true);
//        settings.setResolution(540, 960);               
        settings.setResolution(960, 540);               
        settings.setFrameRate(FPS);
        
        app.setSettings(settings);
        app.setShowSettings(false);
        
        app.start();
    }
}



