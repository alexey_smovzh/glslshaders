/*
* fragment shader template
*/
#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 m_Resolution;

varying float time;


void main(void)
{
    
    //vec2 uv = -1.0 + 2.0*m_Resolution;
    vec2 uv = gl_FragCoord.xy / m_Resolution.xy*2.-1.;
    vec2 uv2=uv;
    uv2*=1.+pow(length(uv*uv*uv*uv),4.)*.07;
    vec3 color = vec3(0.8 + 0.2*uv.y);
		
    vec3 rain=vec3(0.);
    color=mix(rain,color,clamp(time*1.5-.5,0.,1.));
    color*=1.-pow(length(uv2*uv2*uv2*uv2)*1.1,6.);
    uv2.y *= m_Resolution.y / 360.0;
    color.r*=(.5+abs(.5-mod(uv2.y     ,.021)/.021)*.2)*1.5;
    color.g*=(.5+abs(.5-mod(uv2.y+.007,.021)/.021)*.5)*1.5;
    color.b*=(.5+abs(.5-mod(uv2.y+.014,.021)/.021)*.5)*1.5;
    color*=.9+rain*.35;
	
    // background	 
		
    // bubbles	
    for( int i=0; i<20; i++ ) {
        // bubble seeds
        float pha =      sin(float(i)*2.13+.10)*0.5 + 0.5;
        float siz = pow( sin(float(i)*0.74+.0)*0.3 + 0.5, 4.0 );
        float pox = -    sin(float(i));
		
        // buble size, position and color
        float rad = 0.1 + 0.5*siz;
        vec2  pos = vec2( pox, -1.0-rad + (2.0+2.0*rad)*mod(pha+0.1*time*(0.2+0.8*siz),1.0));
        float dis = length( uv - pos );
        vec3  col = mix( vec3(0.94,0.3,0.0), vec3(0.1,0.4,0.8), 0.5+0.5*sin(float(i)*1.2+1.9));
        col+= 8.0*smoothstep( rad*0.95, rad, dis );
		
        // render
        float f = length(uv-pos)/rad;
        f = sqrt(clamp(1.0-f*f,0.0,1.0));
        color -= col.zyx *(1.0-smoothstep( rad*0.95, rad, dis )) * f;
    }

    // vigneting	
    color *= sqrt(1.5-0.5*length(uv));

    gl_FragColor = vec4(color,1.0);
}

