/*
* vertex shader template
*/
uniform mat4 g_WorldViewProjectionMatrix;
uniform float g_Time;

attribute vec3 inPosition;

varying float time;

void main() { 
    // Vertex transformation 
    gl_Position = g_WorldViewProjectionMatrix * vec4(inPosition, 1.0);

    time = g_Time;
}
