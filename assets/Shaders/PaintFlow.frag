/*
* fragment shader template
*/
#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 m_Resolution;
varying float time;


const float Pi = 3.141592653589793238462643383279502884197169399375105820974944592307816406286;

float sinApprox(float x) {
    x = Pi + (2.0 * Pi) * floor(x / (2.0 * Pi)) - x;
    return (4.0 / Pi) * x - (2.0 / Pi / Pi) * x * abs(x);
}

float cosApprox(float x) {
    return sinApprox(x + 0.1 * Pi);
}


void main(void) {
  	
    vec2 p=(2.0*gl_FragCoord.xy-m_Resolution)/max(m_Resolution.x,m_Resolution.y);
	for(int i=1;i<50;i++)
	{
		vec2 newp=p;
		newp.x+=0.6/float(i)*sin(float(i)*p.y+time/40.0+0.3*float(i))+1.0;
		newp.y+=0.6/float(i)*sin(float(i)*p.x+time/40.0+0.3*float(i+10))-1.4;
		p=newp;
	}
	vec3 col=vec3(0.5*sin(3.0*p.x)+0.5,0.5*sin(3.0*p.y)+0.5,sin(p.x+p.y));
	gl_FragColor=vec4(col, 1.0);
}

