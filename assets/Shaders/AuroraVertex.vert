/*
* vertex shader template
*/
uniform mat4 g_WorldViewProjectionMatrix;
uniform mat4 g_WorldMatrix;
uniform float g_Time;

attribute vec3 inPosition;
attribute vec2 inTexCoord;

varying vec2 texCoord;
varying float time;

uniform float m_Wave;
uniform float m_Speed;


void main() { 

    vec4 position = vec4(inPosition, 1.0);
    texCoord = inTexCoord;
    time = g_Time * m_Speed;
    
    position.y = sin(inPosition.x + time) * m_Wave;

    gl_Position = g_WorldViewProjectionMatrix * position;

}
