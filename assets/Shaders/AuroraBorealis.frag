/*
* fragment shader template
*/
#ifdef GL_ES
precision mediump float;
#endif


uniform vec2 m_Resolution;

varying float time;

const vec3 green = vec3(0.0, 1.0, 0.0);
const vec3 blue = vec3(0.0, 0.0, 0.5);


float f(float arg) {
//	return 1.0-sin(arg*70.0+m_Time*0.5)*0.05+sin(arg*25.0+m_Time)*0.1+sin(arg*10.0-m_Time)*0.15;		
        float t = time*0.2;
	return 1.0-sin(arg*70.0+t*0.5)*0.05+sin(arg*25.0+t)*0.1+sin(arg*10.0-t)*0.15;	
}

vec3 c(float d) {
	return (green * (0.7 - d / m_Resolution.x)) + (blue * d);	
}	
 
void main( void ) {

	vec2 position = ( gl_FragCoord.xy / m_Resolution.xy ); 
	position.x *= m_Resolution.x/m_Resolution.y; 
	position.y *= 2.5;		
		
	float x = f(position.x);
	
	vec3 color = position.y>x?c(position.x)-pow((x-position.y), (sin(time)+2.0)*.5):c(position.x)-pow((x-position.y), 0.2);

        gl_FragColor = vec4(color, 1.0);
	
}